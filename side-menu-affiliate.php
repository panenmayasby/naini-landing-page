<div class="tabs_wrapper" id="pin_nav_tab">
    <ul class="nav flex-column nav-tabs-left" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" href="affiliate-url.php">Affiliate URLs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="affiliate-referrals.php">Statistics Graphs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="affiliate-referrals.php">Referrals</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="affiliate-payouts.php">Payouts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="affiliate-visits.php">Visits</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="affiliate-creatives.php">Creatives</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="affiliate-settings.php">Settings</a>
      </li>
    </ul>
</div>