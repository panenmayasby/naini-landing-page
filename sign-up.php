<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Ashekur Rahman">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>Naini Landing Page</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="shortcut icon" type="image/ico" href="images/favicon.ico" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/material-design-iconic-font.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/jquery-jvectormap-2.0.3.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".mainmenu-area">
    <!-- Preloader-content -->
    <div class="preloade">
        <span><i class="zmdi zmdi-compass"></i></span>
    </div>
    <!-- Main-Menu-Area -->
    <nav class="navbar mainmenu-area transparent border-bottom" data-spy="affix" data-offset-top="200">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainmenu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><div class="logo-text">Naini</div></a>
            </div>
            <div class="collapse navbar-collapse" id="mainmenu">
                <div class="navbar-header navbar-right">
                    <a href="#" class="navbar-bttn">Coba Gratis</a>
                    <a href="#" class="navbar-bttn">Login</a>
                </div>
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="#home-page">Beranda</a></li>
                    <li><a href="#about-page">Tentang Kami</a></li>
                    <li><a href="#feature-page">Fitur Kami</a></li>
                    <li><a href="#contact-page">Hubungi Kami</a></li>
                    <li class="active"><a href="#e-training">E-Training</a></li>
                </ul>

            </div>
        </div>
    </nav>
    <!-- Main-Menu-Area / -->
    <section class="info_wrapper affiliate section-padding-top" id="e-training">
        <div class="container">
            <div class="main-title text-center">
                <h3 class="page-title">Affiliate Area</h3>
            </div>
            <div class="main_content">
                <div class="row">
                    <div class="col-md-6 col_login">
                        <div class="login_form_wrapper list_text">
                            <form id="affwp-login-form" class="affwp-form" action="" method="post">
                                <div class="heading-1 mb-5">Log into your account</div>
                                <div class="form-group">
                                    <label for="affwp-login-user-login">Username</label>
                                    <input id="affwp-login-user-login" class="required form-control" type="text" name="affwp_user_login" title="Username">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-login-user-pass">Password</label>
                                    <input id="affwp-login-user-pass" class="password required form-control" type="password" name="affwp_user_pass">
                                </div>
                                <div class="form-check">
                                    <div class="custom-control custom-checkbox pull-left">
                                        <input type="checkbox" class="custom-control-input affwp-user-remember" id="affwp-user-remember">
                                        <label class="affwp-user-remember custom-control-label" for="affwp-user-remember">Remember Me</label>
                                    </div>
                                    <a href="http://naini.id/wp-login.php?action=lostpassword" class="forgot-link pull-right">Lost your password?</a>
                                </div>
                                <input type="hidden" name="affwp_redirect" value="">
                                <input type="hidden" name="affwp_login_nonce" value="199b14922d">
                                <input type="hidden" name="affwp_action" value="user_login">
                                <input type="submit" class="button btn btn-blue btn-submit" value="Log In">
                            </form>
                        </div>
                    </div><!--END COL-->
                    <div class="col-md-6 col_register">
                        <div class="register_form_wrapper list_text">
                            <form id="affwp-register-form" class="affwp-form" action="" method="post">
                                <div class="heading-1 mb-5">Register a new affiliate account</div>
                                <div class="form-group">
                                    <label for="affwp-user-name">Your Name</label>
                                    <input id="affwp-user-name" type="text" name="affwp_user_name" value="" title="Your Name" required="required" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-user-login">Username</label>
                                    <input id="affwp-user-login" required="required" type="text" name="affwp_user_login" maxlength="60" value="" title="Username" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-user-email">Account Email</label>
                                    <input id="affwp-user-email" required="required" type="email" name="affwp_user_email" value="" title="Email Address" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-payment-email">Payment Email</label>
                                    <input id="affwp-payment-email" type="email" name="affwp_payment_email" value="" title="Payment Email Address" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-user-url">Website URL</label>
                                    <input id="affwp-user-url" type="text" name="affwp_user_url" value="" title="Website URL" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-promotion-method">How will you promote us?</label>
                                    <textarea id="affwp-promotion-method" name="affwp_promotion_method" rows="5" cols="30" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="affwp-user-pass">Password</label>
                                    <input id="affwp-user-pass" required="required" class="password form-control" type="password" name="affwp_user_pass">
                                </div>
                                <div class="form-group">
                                    <label for="affwp-user-pass2">Confirm Password</label>
                                    <input id="affwp-user-pass2" required="required" class="password form-control" type="password" name="affwp_user_pass2">
                                </div>
                                <div class="form-group">
                                    <input type="hidden" name="affwp_honeypot" value="">
                                    <input type="hidden" name="affwp_redirect" value="">
                                    <input type="hidden" name="affwp_register_nonce" value="ca6a2d89c2">
                                    <input type="hidden" name="affwp_action" value="affiliate_register">
                                    <input class="button btn btn-blue btn-submit" type="submit" value="Register">
                                </div>
                            </form>
                        </div>
                    </div><!--END COL-->
                </div>
            </div><!--END CONTENT-->
        </div>
    </section>
    <!-- Footer-Area -->
    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3">
                    <div class="logo-text">Naini</div>
                    <div class="space-20"></div>
                    <p>Even when carefully kept, paper journals can be read by anyone who happens.</p>
                    <div class="space-20"></div>
                    <ul class="social-menu">
                        <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                        <li><a href="#"><i class="zmdi zmdi-youtube-play"></i></a></li>
                    </ul>
                    <div class="space-20"></div>
                    <p>Copyright Naini. 2018</p>
                    <div class="space-50"></div>
                </div>
                <div class="col-xs-12 col-sm-2">
                    <h4>About</h4>
                    <div class="space-10"></div>
                    <ul class="list">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Partners</a></li>
                        <li><a href="#">Career</a></li>
                        <li><a href="#">Reviews</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li><a href="#">Help</a></li>
                    </ul>
                    <div class="space-50"></div>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <h4>Contact</h4>
                    <div class="space-10"></div>
                    <ul class="icon-list">
                        <li>
                            <div class="icon">
                                <i class="zmdi zmdi-pin zmdi-hc-fw"></i>
                            </div>
                            <strong>Location</strong> : Surabaya
                        </li>
                        <li>
                            <div class="icon">
                                <i class="zmdi zmdi-email"></i>
                            </div>
                            <strong>Mail</strong> : naini@mail.com
                        </li>
                        <li>
                            <div class="icon">
                                <i class="zmdi zmdi-phone-in-talk"></i>
                            </div>
                            <strong>Phone</strong> : +5569 187 9852
                        </li>
                    </ul>
                    <div class="space-50"></div>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <h4>Newsletter</h4>
                    <div class="space-10"></div>
                    <p>Don’t Worry We Won’t Spam in your inbox</p>
                    <div class="space-20"></div>
                    <form id="mc-form" class="subscribe">
                        <input type="text" class="control" placeholder="Email" id="mc-email">
                        <button class="sub-button" type="submit"><i class="zmdi zmdi-long-arrow-right"></i></button>
                        <label class="mt10" for="mc-email"></label>
                    </form>
                    <div class="space-50"></div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer-Area / -->
    <!--Vendor-JS-->
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <!--SCROLL MAGIC-->
    <script src="js/scrollmagic/ScrollMagic.js"></script>
    <script type="text/javascript">
        var controller = new ScrollMagic.Controller();
        toggleScenes();
      $(function () { // wait for document ready
        // build scene
        if($(window).width() >= 764 ){
            if(($('.tab-content').height() - $("#pin_nav_tab").height()) > 0){
                var scene = new ScrollMagic.Scene({triggerElement: "#pin_nav_tab", duration: $('.tab-content').height() - $("#pin_nav_tab").height(), triggerHook:0.2})
                .setPin("#pin_nav_tab")
                .addTo(controller);                
            }
        }
    });
      function toggleScenes(){
        if($(window).width() >= 764 ){
          controller.enabled();

      } else if ( controller ){
          controller.enabled(false);
      }
  }
</script>
<!--Plugin-JS-->
<script src="js/owl.carousel.min.js"></script>
<script src="js/contact-form.js"></script>
<script src="js/scrollUp.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/magnific-popup.min.js"></script>
<script src="js/imagesloaded.js"></script>
<script src="js/ajaxchimp.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery-jvectormap-2.0.3.min.js"></script>
<script src="js/maps.js"></script>
<!--Main-active-JS-->
<script src="js/main.js"></script>

</body>

</html>